package com.example.demo.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/5/31 17:29
 */
@RestController
@EnableAutoConfiguration
public class Example {
    @RequestMapping("/")
    public String hello() {
        return "hello world!";
    }
}
