package com.zhangxh.springbootjdbc.repository;

import com.zhangxh.springbootjdbc.pojo.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/6/12 22:37
 */
public interface UserRepository extends JpaRepository<User,Long> {
    User findByName(String name);

    User findByNameAndAge(String name, Integer age);

    @Query("from User  where name=:name")
    User findUser(@Param("name") String name);
}
