package com.zhangxh.springbootjdbc.repository;

import com.zhangxh.springbootjdbc.pojo.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/6/12 22:40
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;

    @Test
    public void test() throws Exception {
        userRepository.save(new User("AAA",10));
        userRepository.save(new User("BBB",10));
        userRepository.save(new User("CCC",10));
        userRepository.save(new User("DDD",10));
        userRepository.save(new User("EEE",10));
        userRepository.save(new User("FFF",10));
        userRepository.save(new User("GGG",10));
        Assert.assertEquals(7, userRepository.findAll().size());
    }
}