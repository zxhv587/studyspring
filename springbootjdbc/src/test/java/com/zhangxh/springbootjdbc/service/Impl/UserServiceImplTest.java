package com.zhangxh.springbootjdbc.service.Impl;

import com.zhangxh.springbootjdbc.service.UserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/6/12 22:17
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    @Autowired
    private UserService userService;

    @Before
    public void setUp() {
        userService.deleteAllUsers();

    }

    @Test
    public void test() throws Exception {
        userService.create("a", 1);
        userService.create("b", 2);
        userService.create("c", 3);
        userService.create("d", 4);
        Assert.assertEquals(4, userService.getAllUsers().intValue());
        userService.deleteByName("b");
        userService.deleteByName("d");
        Assert.assertEquals(2, userService.getAllUsers().intValue());
    }
    @Test
    public void create() {
    }

    @Test
    public void deleteByName() {
    }

    @Test
    public void getAllUsers() {
    }

    @Test
    public void deleteAllUsers() {
    }
}