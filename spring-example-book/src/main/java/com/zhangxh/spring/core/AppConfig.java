package com.zhangxh.spring.core;

import org.springframework.context.annotation.Configuration;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/7/12 23:13
 */
@Configuration
public class AppConfig {
    public MyService myService() {
        return new MyServiceImpl();
    }
}
