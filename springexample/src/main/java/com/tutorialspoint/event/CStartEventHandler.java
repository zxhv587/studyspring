package com.tutorialspoint.event;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextStartedEvent;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/5/31 13:48
 */
public class CStartEventHandler implements ApplicationListener<ContextStartedEvent> {
    public void onApplicationEvent(ContextStartedEvent contextStartedEvent) {
        System.out.println("ContextStartedEvent Received");
    }
}
