package com.tutorialspoint;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/5/23 18:47
 */
public class HelloWorld {

    private String message;
    public void setMessage(String message){
        this.message  = message;
    }
    public void getMessage(){
        System.out.println("Your Message : " + message);
    }
}
