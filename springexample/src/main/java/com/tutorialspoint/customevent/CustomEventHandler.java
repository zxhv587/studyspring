package com.tutorialspoint.customevent;

import org.springframework.context.ApplicationListener;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/5/31 14:19
 */
public class CustomEventHandler implements ApplicationListener<CustomEvent> {
    public void onApplicationEvent(CustomEvent customEvent) {
        System.out.println(customEvent.toString());
    }
}
