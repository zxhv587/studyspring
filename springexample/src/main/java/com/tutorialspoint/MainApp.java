package com.tutorialspoint;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 说明
 *
 * @author zhangxh
 * @email zhangxh@cheegu.com
 * @Time 2018/5/28 23:24
 */
public class MainApp {
    public static void main(String[] args) {
      /*  ApplicationContext context = new FileSystemXmlApplicationContext("F:\\study\\spring\\studyspring\\springexample\\src\\main\\resources\\Beans.xml");
        HelloWorld obj = (HelloWorld) context.getBean("helloWorld");
        obj.getMessage();*/
        ApplicationContext context1=new ClassPathXmlApplicationContext("Beans.xml");
        SingletonHelloWorld helloWorld = (SingletonHelloWorld) context1.getBean("singletonHelloWorld");
        helloWorld.setMessage("I'm object A");
        helloWorld.getMessage();
        SingletonHelloWorld helloWorld1=(SingletonHelloWorld)context1.getBean("singletonHelloWorld");
        helloWorld1.getMessage();

    }
}
