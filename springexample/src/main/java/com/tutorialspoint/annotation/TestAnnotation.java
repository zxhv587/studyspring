package com.tutorialspoint.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 注解学习
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/6/13 23:20
 */
@Target(ElementType.METHOD)//Target标识注解应该用在什么地方
@Retention(RetentionPolicy.RUNTIME)//标识应该用在哪个级别 source源文件  class 类文件 runtime 运行时
public @interface TestAnnotation {

}
