package com.tutorialspoint.annotation;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/5/31 11:59
 */
public class HelloWorld {
    private  String message;

    @TestAnnotation
    public void getMessage() {
        System.out.println("Your Message :" + message);
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
