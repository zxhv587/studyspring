package com.tutorialspoint.annotation;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/5/31 11:59
 */
@Configuration
public class HelloWorldConfig {
    @Bean
    public HelloWorld helloWorld() {
        return  new HelloWorld();
    }
}
