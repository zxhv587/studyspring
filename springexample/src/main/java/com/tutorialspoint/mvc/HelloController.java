package com.tutorialspoint.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/5/31 15:03
 */
@Controller
@RequestMapping("/hello")
public class HelloController {
    public String printHello(ModelMap modelMap) {
        modelMap.addAttribute("message", "Hello Spring MVC FrameWork");
        return "hello";
    }
}
