package com.tutorialspoint.prototype;

import com.tutorialspoint.SingletonHelloWorld;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 说明
 *
 * @author zhangxh
 * @email zhangxh@cheegu.com
 * @Time 2018/5/29 0:13
 */
public class MainApp {
    public static void main(String[] args) {
        ApplicationContext context1=new ClassPathXmlApplicationContext("Beans.xml");
        SingletonHelloWorld helloWorld = (SingletonHelloWorld) context1.getBean("prototypeHelloWorld");
        helloWorld.setMessage("I'm object A");
        helloWorld.getMessage();
        SingletonHelloWorld helloWorld1=(SingletonHelloWorld)context1.getBean("prototypeHelloWorld");
        helloWorld1.getMessage();

    }
}
