package com.tutorialspoint.di.args;

/**
 * 说明
 *
 * @author zhangxh
 * @email zhangxh@cheegu.com
 * @Time 2018/5/31 0:20
 */
public class TextEditor {
    public SpellChecker getSpellChecker() {
        return spellChecker;
    }

    public void setSpellChecker(SpellChecker spellChecker) {
        System.out.println("Inside setSpellChecker");
        this.spellChecker = spellChecker;
    }

    private SpellChecker spellChecker;

   /* public TextEditor(SpellChecker spellChecker) {
        System.out.println("Inside setSpellChecker");
        this.spellChecker = spellChecker;
    }
*/


    public void spellCheck() {
        spellChecker.checkSpelling();
    }
}
