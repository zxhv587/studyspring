package com.tutorialspoint.di;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 说明
 *
 * @author zhangxh
 * @email zhangxh@cheegu.com
 * @Time 2018/5/31 0:32
 */
public class MainApp {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("innerBeans.xml");
        TextEditor te = (TextEditor) context.getBean("textEditor");
        te.spellChek();
    }
}
