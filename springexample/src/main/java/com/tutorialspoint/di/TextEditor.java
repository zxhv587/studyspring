package com.tutorialspoint.di;

/**
 * 说明
 *
 * @author zhangxh
 * @email zhangxh@cheegu.com
 * @Time 2018/5/31 0:20
 */
public class TextEditor {
    private  SpellCheker spellChecker;

    public TextEditor(SpellCheker spellCheker) {
        System.out.println("Inside TextEditor constructor");
        this.spellChecker = spellCheker;
    }

    public void spellChek() {
        spellChecker.checkSpelling();
    }


}
