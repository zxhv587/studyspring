package com.tutorialspoint;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;

/**
 * 说明
 *
 * @author zhangxh
 * @email zhangxh@cheegu.com
 * @Time 2018/5/24 1:09
 */
public class AppMain {
    public static void main(String[] arg) {
        XmlBeanFactory factory = new XmlBeanFactory
                (new ClassPathResource("Beans.xml"));
        HelloWorld obj = (HelloWorld) factory.getBean("helloWorld");
        obj.getMessage();
    }
}
