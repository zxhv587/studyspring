package com.zhangxh.springexample.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/6/5 10:17
 */
@Component
public class MyConfig {

    @Value("${com.zhangxh.springexample.name}")
    private  String name;
    @Value("${com.zhangxh.springexample.title}")
    private String title;

    public int getRandomvalue() {
        return randomvalue;
    }

    public void setRandomvalue(int randomvalue) {
        this.randomvalue = randomvalue;
    }

    @Value("${com.zhangxh.springexample.randomvalue}")
    private int randomvalue;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
