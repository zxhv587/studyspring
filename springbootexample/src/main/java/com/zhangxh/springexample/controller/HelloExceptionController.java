package com.zhangxh.springexample.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 演示异常消息
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/6/6 22:42
 */
@Controller
@RequestMapping("/exception")
public class HelloExceptionController {

    @RequestMapping("/hello")
    public String hello() throws Exception {
        throw new Exception("发生错误");
    }
}
