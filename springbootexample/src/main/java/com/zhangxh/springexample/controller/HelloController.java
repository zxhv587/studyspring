package com.zhangxh.springexample.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/6/5 10:56
 */
@RestController
public class HelloController {
    @ApiOperation("测试接口")
    @RequestMapping("/hello")
    public String index() {
        return "hello world!";
    }
}
