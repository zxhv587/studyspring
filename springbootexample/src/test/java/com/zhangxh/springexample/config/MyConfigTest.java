package com.zhangxh.springexample.config;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/6/5 10:24
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class MyConfigTest {
    private static final Log log = LogFactory.getLog(MyConfigTest.class);
    @Autowired
    MyConfig myConfig;
    @Test
    public  void  getConfig()throws  Exception {
        Assert.assertEquals(myConfig.getName(),"程序猿DD");
        Assert.assertEquals(myConfig.getTitle(),"Spring Boot教程");
        log.info(myConfig.getRandomvalue());

    }

}