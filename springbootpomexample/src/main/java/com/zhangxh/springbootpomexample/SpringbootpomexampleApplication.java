package com.zhangxh.springbootpomexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//@EnableAutoConfiguration
public class SpringbootpomexampleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootpomexampleApplication.class, args);
    }
}
