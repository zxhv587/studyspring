package com.zhangxh.springbootpomexample.controller;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 说明
 *
 * @author: zhangxh
 * @email: zhangxh@cheegu.com
 * @date: 2018/6/7 23:35
 */
@RestController

public class Example {
    @RequestMapping("/")
    String home() {
        return "Hello World!";
    }
}
