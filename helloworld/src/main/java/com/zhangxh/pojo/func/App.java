package com.zhangxh.pojo.func;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 函数式编程示例
 * Stream 的方法分为两类。一类叫惰性求值，一类叫及早求值。
 * 判断一个操作是惰性求值还是及早求值很简单：只需看它的返回值。如果返回值是 Stream，那么是惰性求值。
 * 其实可以这么理解，如果调用惰性求值方法，Stream 只是记录下了这个惰性求值方法的过程，并没有去计算，等到调用及早求值方法后，就连同前面的一系列惰性求值方法顺序进行计算，返回结果。
 */
public class App {
    public static void main(String args[]) {
        //List<A> to Map<Long,string>

        List<StudentA> studentAS = getDefaultStudents();
        System.out.println("是否打印了数据studentAS");
        //studentAS
        System.out.println("获取数据studentAS:" + JSONObject.toJSON(studentAS));
        Map<Integer, String> map1 = studentAS.stream().collect(Collectors.toMap(StudentA::getId, StudentA::getName));
        //打印了数据出来
        System.out.println("是否打印了数据map1");
        System.out.println("取名称并转换成大写map1:" + JSONObject.toJSON(map1));
        List<String> list = studentAS.stream().map(p -> p.getName()).collect(Collectors.toList());
        System.out.println("是否打印了数据list");
        System.out.println("取名称并转换成大写list:" + JSONObject.toJSON(map1));
        List<String> strings = studentAS.stream().map(p -> p.getName().toUpperCase()).collect(Collectors.toList());
        System.out.println("是否打印了数据strings");
        System.out.println("取名称并转换成大写strings:" + JSONObject.toJSON(strings));
        Map<Integer, StudentA> map = studentAS.stream().collect(Collectors.toMap(StudentA::getId, p -> p));
        System.out.println("是否打印了数据map");
        System.out.println("转换成map:" + JSONObject.toJSON(map));
        Map<Integer, List<StudentA>> mapClasss = studentAS.stream().collect(Collectors.groupingBy(StudentA::getClassId));
        //并没有打印实际的,没有调用getName方法
        System.out.println("是否打印了数据mapClasss");
        System.out.println("转换成mapClasss:" + JSONObject.toJSON(mapClasss));
        Map<Integer, Map<Integer, StudentA>> mapMap = mapClasss.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                p -> p.getValue().stream().collect(Collectors.toMap(StudentA::getId, Function.identity()))));
        System.out.println("是否打印了数据mapMap");
        System.out.println("转换成mapMap:" + JSONObject.toJSON(mapMap));
        Map<Integer, Map<Integer, StudentTo>> mapMap2 = mapClasss.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey,
                p -> p.getValue().stream().collect(Collectors.toMap(StudentA::getId, to ->
                        new StudentTo(to.getId(), to.getName())
                ))));
        System.out.println("是否打印了数据mapMap2");
        System.out.println("转换成mapMap2:" + JSONObject.toJSON(mapMap2));
//        Map<Integer, Integer> map2 = studentAS.stream().collect(Collectors.toMap(StudentA::getId, StudentA::getId));
//        Map<Integer, String> map3 = map2.entrySet().stream().collect(Collectors.toMap(Map.Entry::getKey, p -> p.toString().toUpperCase()));


    }

    public static List<StudentA> getDefaultStudents() {
        List<StudentA> result = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            System.out.println("生成学生信息" + i);
            result.add(new StudentA(i, i, String.format("Class:%s,student:%s", i, i)));
        }
        return result;
    }

    public static List<Classs> getDefaultClass() {
        List<Classs> result = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            result.add(new Classs(i, String.format("班级%s", i)));
        }
        return result;
    }
}
