package com.zhangxh.pojo.func;

/**
 * 说明
 */
public class StudentA {
    private String name;
    private Integer id;
    private Integer classId;

    public StudentA(Integer classId, Integer id, String name) {
        this.classId = classId;
        this.id = id;
        this.name = name;
    }
    public String getName() {
        System.out.println("调用了name" + name);
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }
}
