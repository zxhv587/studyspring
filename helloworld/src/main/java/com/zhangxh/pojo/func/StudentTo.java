package com.zhangxh.pojo.func;

/**
 * 说明
 */
public class StudentTo {
    private Integer id;
    private String name;

    public StudentTo(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
