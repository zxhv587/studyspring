package com.zhangxh;

/**
 * 说明
 *
 * @author zhangxh
 * @email zhangxh@cheegu.com
 * @Time 2018/5/24 1:20
 */
public class HelloWorld {
    private String message;

    public void setMessage(String message) {
        this.message = message;
    }

    public void getMessage() {
        System.out.println("Your Message : " + message);
    }
}
